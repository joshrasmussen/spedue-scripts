#!/usr/bin/env node

import path from 'path'
import crossSpawn from 'cross-spawn'
import chalk from 'chalk'
import glob from 'glob'

const args = process.argv.slice(2)

const scripts = glob
  .sync(path.join(__dirname, '..', 'scripts', '*'))
  .map(f => path.parse(f).name)
  .filter(f => !f.includes('.'))

const scriptIndex = args.findIndex(x => x === 'test' || x === 'build')
const script = scriptIndex === -1 ? args[0] : args[scriptIndex]
const nodeArgs = scriptIndex > 0 ? args.slice(0, scriptIndex) : []

if (scripts.includes(script)) {
  const result = crossSpawn.sync(
    'node',
    [
      ...nodeArgs,
      require.resolve(`../scripts/${script}`),
      ...args.slice(scriptIndex !== -1 ? scriptIndex + 1 : 1),
    ],
    { stdio: 'inherit' }
  )

  if (result.signal && ['SIGKILL', 'SIGTERM'].includes(result.signal)) {
    console.log(
      chalk.yellow(
        `    ⚠️ The build failed because the process exited early ⚠️`
      )
    )

    process.exit(1)
  }

  process.exit(result.status || 0)
} else {
  console.log(chalk.red(`I don't know what the ${script} script does 😞`))
}
